package ru.sber.jd.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class RegistrationDBTableEntity {

    @Column
    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer id;
    @Column
    private String login;
    @Column
    private Integer hashPassword;
    @Column
    private String email;
    @Column
    private String userRole;
    @Column
    private String token;
    @Column
    private LocalDateTime tokenEndLifeTime;

}
