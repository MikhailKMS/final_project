package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.ResponseRecoveryDto;
import ru.sber.jd.dto.ResponseRegistrationDto;
import ru.sber.jd.dto.UserDto;
import ru.sber.jd.entities.RegistrationDBTableEntity;
import ru.sber.jd.repositories.RegistrationDBTableRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Класс для работы с БД пользователей: регстрация новых, авторизация зарегистрированных, проерка токенов, восстановление пароля
 */
@Service
@RequiredArgsConstructor
public class RegistrationDBTableService {

    public static String BOOTSTRAP_SERVER = "localhost:9092";

    private final RegistrationDBTableRepository registrationDBTableRepository;

    private RegistrationDBTableEntity save(RegistrationDBTableEntity entity) {
        return registrationDBTableRepository.save(entity);
    }

    private String tokenGenerator(int tokenLength) {
        Random r = new Random();
        String dict = "abcdefghijklmnopqrstuvwxyz0123456789";

        String randomToken = "" + dict.charAt(r.nextInt(dict.length()));
        for (int i = 0; i < tokenLength - 1; i++) {
            randomToken = randomToken + dict.charAt(r.nextInt(dict.length()));
        }

        return randomToken;
    }

    /**
     * Метод регистрирует нового пользователя
     * @param userDto для регистрации необходимо передать логин, пароль, email
     * @return ResponseRegistrationDto - код ошибки, описание ошибки, токен (20 символов)
     * Список возможных возращаемых ошибок для данного метода:
     * 0 - Пользователь успешно зарегистрирован, возвращается актуальный токен (время жизни 1 минута)
     * 1 - Пользователь с таким логином уже зарегистрирован, токен не возвращается (null)
     * 2 - Указанный Email принадлежит другому пользователю, токен не возвращается (null)
     * 3 - Дополнительно укажите Email для восстановления пароля, токен не возвращается (null)
     */
    public ResponseRegistrationDto registrationNewUser(UserDto userDto) {
        //проверка существования такого пользователя
        List<RegistrationDBTableEntity> allUserInTable = registrationDBTableRepository.findAll();

        boolean flag = false; //нет такого пользователя
        for (RegistrationDBTableEntity oneUserInTable : allUserInTable) {
            if (oneUserInTable.getLogin().equals(userDto.getLogin())) flag = true;
        }

        ResponseRegistrationDto responseRegistrationDto = new ResponseRegistrationDto();

        boolean flagEmail = false; //такой email не зарегистрирован
        if (!flag & userDto.getEmail() != null) {
            for (RegistrationDBTableEntity oneUserInTable : allUserInTable) {
                if (oneUserInTable.getEmail().equals(userDto.getEmail())) {
                    flagEmail = true;
                    //генерация ответа
                    responseRegistrationDto.setCodeError(2);
                    responseRegistrationDto.setErrorString("Указанный Email принадлежит другому пользователю");
                    responseRegistrationDto.setToken(null);
                }
            }
        }

        if (!flag & !flagEmail & userDto.getEmail() != null) {

            //создание объекта
            String generatedToken = tokenGenerator(20);
            RegistrationDBTableEntity registrationDBTableEntity = new RegistrationDBTableEntity();
            registrationDBTableEntity.setLogin(userDto.getLogin());
            registrationDBTableEntity.setHashPassword(Objects.hashCode(userDto.getPassword()));
            registrationDBTableEntity.setEmail(userDto.getEmail());
            registrationDBTableEntity.setUserRole("user");
            registrationDBTableEntity.setToken(generatedToken);
            registrationDBTableEntity.setTokenEndLifeTime(LocalDateTime.now().plusMinutes(1L));

            //запись в базу
            save(registrationDBTableEntity);

            //генерация ответа
            responseRegistrationDto.setCodeError(0);
            responseRegistrationDto.setErrorString("Пользователь успешно зарегистрирован");
            responseRegistrationDto.setToken(generatedToken);

        } else if (!flagEmail) {
            //генерация ответа
            responseRegistrationDto.setCodeError(1);
            responseRegistrationDto.setErrorString("Пользователь с таким логином уже зарегистрирован");
            responseRegistrationDto.setToken(null);
        }

        if (userDto.getEmail() == null) {
            //генерация ответа
            responseRegistrationDto.setCodeError(3);
            responseRegistrationDto.setErrorString("Дополнительно укажите Email для восстановления пароля");
            responseRegistrationDto.setToken(null);
        }

        return responseRegistrationDto;
    }


    /**
     * Метод проверяет зарегистрирован ли в базе указанный пользователь и при положительном ответе возвращает токен
     * @param userDto для авторизации необходимо передать логин и пароль. Email не проверяется
     * @return ResponseRegistrationDto - код ошибки, описание ошибки, токен (20 символов)
     * Список возможных возращаемых ошибок для данного метода:
     * 4 - Пользователя с таким логином не существует, токен не возвращается (null)
     * 5 - Неверный пароль, токен не возвращается (null)
     * 6 - Проверка пользователя успешна, возвращается актуальный токен (время жизни 1 минута)
     */
    public ResponseRegistrationDto authentificationUser(UserDto userDto) {

        List<RegistrationDBTableEntity> allUserInTable = registrationDBTableRepository.findAll();

        ResponseRegistrationDto responseRegistrationDto = new ResponseRegistrationDto();

        responseRegistrationDto.setCodeError(4);
        responseRegistrationDto.setErrorString("Пользователя с таким логином не существует");
        responseRegistrationDto.setToken(null);

        for (RegistrationDBTableEntity oneUserInTable : allUserInTable) {

            if (oneUserInTable.getLogin().equals(userDto.getLogin())) {
                responseRegistrationDto.setCodeError(5);
                responseRegistrationDto.setErrorString("Неверный пароль");
                responseRegistrationDto.setToken(null);

                if (oneUserInTable.getHashPassword().equals(Objects.hashCode(userDto.getPassword()))) {

                    oneUserInTable.setToken(tokenGenerator(20));
                    oneUserInTable.setTokenEndLifeTime(LocalDateTime.now().plusMinutes(1L));
                    save(oneUserInTable);

                    responseRegistrationDto.setCodeError(6);
                    responseRegistrationDto.setErrorString("Проверка пользователя успешна");
                    responseRegistrationDto.setToken(oneUserInTable.getToken());
                }
            }
        }

        return responseRegistrationDto;
    }

    /**
     * Метод проверяет наличие представленного токена у любого пользователя в базе, а так же время его жизни
     * @param userToken для проверки токена необхолимо передать токен
     * @return ResponseRegistrationDto - код ошибки, описание ошибки, токен (20 символов)
     * Список возможных возращаемых ошибок для данного метода:
     * 7 - Токен не существует. Необходимо ввести логин и пароль, токен не возвращается (null)
     * 8 - Токен просрочен. Необходимо ввести логин и пароль, токен не возвращается (null)
     * 9 - Токен актуален, токен не возвращается (null)
     */
    public ResponseRegistrationDto checkUserToken(String userToken) {

        List<RegistrationDBTableEntity> allUserInTable = registrationDBTableRepository.findAll();

        ResponseRegistrationDto responseRegistrationDto = new ResponseRegistrationDto();

        responseRegistrationDto.setCodeError(7);
        responseRegistrationDto.setErrorString("Токен не существует. Необходимо ввести логин и пароль");
        responseRegistrationDto.setToken(null);

        for (RegistrationDBTableEntity oneUserInTable : allUserInTable) {
            if (oneUserInTable.getToken().equals(userToken)) {
                responseRegistrationDto.setCodeError(8);
                responseRegistrationDto.setErrorString("Токен просрочен. Необходимо ввести логин и пароль");
                responseRegistrationDto.setToken(null);

                if (oneUserInTable.getTokenEndLifeTime().compareTo(LocalDateTime.now()) > 0) {
                    responseRegistrationDto.setCodeError(9);
                    responseRegistrationDto.setErrorString("Токен актуален");
                    responseRegistrationDto.setToken(null);
                }

            }
        }

        return responseRegistrationDto;
    }


    /**
     * Метод восстанавливает пароль, путем генерации нового
     * @param userDto для восстановления пароля необходимо передать логин. Пароль и Email не проверяется
     * @return ResponseRecoveryDto - код ошибки, описание ошибки, пароль (10 символов), email
     * Список возможных возращаемых ошибок для данного метода:
     * 9 - такого пользователя не существует. Укажите корректный логин, пароль и email не возвращаются (null)
     * 10 - Пользователь найден. Пароль изменен в базе, возвращаются пароль и email
     */
    public ResponseRecoveryDto recoveryPassword(UserDto userDto) {

        List<RegistrationDBTableEntity> allUserInTable = registrationDBTableRepository.findAll();

        ResponseRecoveryDto responseRecoveryDto = new ResponseRecoveryDto();

        responseRecoveryDto.setCodeError(9);
        responseRecoveryDto.setErrorString("Такого пользователя не существует. Укажите корректный логин");


        for (RegistrationDBTableEntity oneUserInTable : allUserInTable) {
            if (oneUserInTable.getLogin().equals(userDto.getLogin())) {
                //меняем пароль в базе
                String generatedPassword = tokenGenerator(10);
                oneUserInTable.setHashPassword(Objects.hashCode(generatedPassword));
                oneUserInTable.setTokenEndLifeTime(LocalDateTime.now().minusMinutes(1L));
                save(oneUserInTable);
                //генерация ответа
                responseRecoveryDto.setCodeError(10);
                responseRecoveryDto.setErrorString("Пользователь найден. Пароль изменен в базе");
                responseRecoveryDto.setPassword(generatedPassword);
                responseRecoveryDto.setEmail(oneUserInTable.getEmail());
            }
        }

        return responseRecoveryDto;
    }


}
