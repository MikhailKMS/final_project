package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.RegistrationDBTableEntity;

@Repository
public interface RegistrationDBTableRepository extends JpaRepository<RegistrationDBTableEntity, Integer> {
}
