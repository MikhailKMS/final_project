package ru.sber.jd.dto;

import lombok.Data;

@Data
public class ResponseRegistrationDto {

    private Integer codeError;
    private String errorString;
    private String token;

}
