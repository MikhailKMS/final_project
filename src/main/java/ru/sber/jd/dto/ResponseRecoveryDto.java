package ru.sber.jd.dto;

import lombok.Data;

@Data
public class ResponseRecoveryDto {

    private Integer codeError;
    private String errorString;
    private String password;
    private String email;
}
